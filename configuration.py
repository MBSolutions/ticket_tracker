from trytond.model import ModelView, ModelSQL, ModelSingleton, fields
from trytond.wizard import Wizard
from trytond.pyson import Not, Bool, Eval
from trytond.modules.ticket_tracker import TrackerHandler
from trytond.pool import Pool


class Configuration(ModelSingleton, ModelView, ModelSQL):
    "Ticket Configuration"
    _name = "ticket.configuration"
    _description = __doc__

    trackers = fields.One2Many('ticket.tracker', 'configuration', 'Trackers')

Configuration()


class CheckTrackerResult(ModelView):
    'Check Tracker - Check'
    _name = 'ticket.configuration.check_tracker.result'
    _description = __doc__
    tracker_succeed = fields.Many2Many('ticket.tracker',
            None, None, 'Tracker Succeed', readonly=True, states={
                'invisible': Not(Bool(Eval('tracker_succeed'))),
                }, depends=['tracker_succeed'])
    tracker_failed = fields.Many2Many('ticket.tracker',
            None, None, 'Tracker Failed', readonly=True, states={
                'invisible': Not(Bool(Eval('tracker_failed'))),
                }, depends=['tracker_failed'])
    tracker_skip = fields.Many2Many('ticket.tracker',
            None, None, 'Tracker Skiped', readonly=True, states={
                'invisible': Not(Bool(Eval('tracker_skip'))),
                }, depends=['tracker_skip'])
CheckTrackerResult()


class CheckTracker(Wizard):
    'Check Tracker'
    _name = 'ticket.configuration.check_tracker'
    states = {
        'init': {
            'result': {
                'type': 'choice',
                'next_state': '_choice',
            },
        },
        'check': {
            'actions': ['_check'],
            'result': {
                'type': 'form',
                'object': 'ticket.configuration.check_tracker.result',
                'state': [
                    ('end', 'Ok', 'tryton-ok', True),
                ],
            },
        },
    }

    def _choice(self, data):
        return 'check'

    def _check(self, data):
        tracker_obj = Pool().get('ticket.tracker')
        config_obj =  Pool().get('ticket.configuration')

        config = config_obj.browse(config_obj.get_singleton_id())
        res = {
            'tracker_succeed': [],
            'tracker_failed': [],
            'tracker_skip': [],
        }
        for tracker in config.trackers:
            if not tracker.active:
                res['tracker_skip'].append(tracker.id)
                continue
            tracker_handler = TrackerHandler.get_handler(tracker)
            if tracker_handler.check_connection():
                res['tracker_succeed'].append(tracker.id)
            else:
                res['tracker_failed'].append(tracker.id)
        return res

CheckTracker()



